package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repository.CammsBORepository;
import com.example.demo.EntityModel.CammsBO;
//import com.example.demo.EntityModel.ErrorCammsBO;

@RestController
@RequestMapping(path="/api/cammsbo")
@CrossOrigin(origins = "*")
public class cammBOController {

	
	@Autowired CammsBORepository  cammsBORepository;
	
	@GetMapping(path="/processCAMMSData")
	public ResponseEntity<Object> processCAMMSData()
	{
		List<CammsBO> successCammsBO = new ArrayList<CammsBO>();
		//List<ErrorCammsBO> failureCammsBO = new ArrayList<ErrorCammsBO>();
		
		List<CammsBO> initialCammsBO = new ArrayList<CammsBO>();
		
		String strLine = "";
		
		//File file = new File("sampleCSV.csv");
		try(FileReader fileReader = new FileReader("test.csv");
				BufferedReader reader = new BufferedReader(fileReader);)
		{
			
			
 			while ((strLine = reader.readLine()) != null)
			{
 				CammsBO cammsBO = new CammsBO();
 				
 				String[] fieldsArray = strLine.split("\\|", -1);
 				
 				cammsBO.setBENumber(fieldsArray[0]);
 				cammsBO.setBECategory(fieldsArray[1]);
 				cammsBO.setClinicCategory(fieldsArray[2]);
 				cammsBO.setZone(fieldsArray[3]);
 				cammsBO.setClinicCode(fieldsArray[4]);
 				cammsBO.setDistrict(fieldsArray[5]);
 				cammsBO.setCircle(fieldsArray[6]);
 				cammsBO.setInstallationDate(convertDate(fieldsArray[7]));
 				cammsBO.setTCDate(convertDate(fieldsArray[8]));
 				cammsBO.setAcceptanceDate(convertDate(fieldsArray[9]));
 				cammsBO.setRentalStartDate(convertDate(fieldsArray[10]));
 				cammsBO.setRentalEndDate(convertDate(fieldsArray[11]));
 				cammsBO.setWarrantyExpiryDate(convertDate(fieldsArray[12]));
 				cammsBO.setWarrantyStartDate(convertDate(fieldsArray[13]));
 				cammsBO.setPurchaseDate(convertDate(fieldsArray[14]));
 				cammsBO.setClinicType(fieldsArray[15]);
 				cammsBO.setState(fieldsArray[16]);
 				cammsBO.setMonthlyRev(Float.parseFloat(fieldsArray[17]));
 				if(!fieldsArray[18].equals(""))
 				{
 				cammsBO.setRentalPerMonth(Float.parseFloat(fieldsArray[18]));
 				}
 				else
 				{cammsBO.setRentalPerMonth(0);}	
 				cammsBO.setBatchNumber(fieldsArray[19]);
 				cammsBO.setOwnership(fieldsArray[20]);
 				cammsBO.setPurchaseCost(Float.parseFloat(fieldsArray[21]));
 				cammsBO.setModelNumber(fieldsArray[22]);
 				cammsBO.setSerialNumber(fieldsArray[23]);
 				cammsBO.setCurrentInstallmentNo(fieldsArray[24]);
 				cammsBO.setBEConditionalStatus(fieldsArray[25]);
 				cammsBO.setAuditStartDate(convertDate(fieldsArray[26]));
 			
 				if(fieldsArray[20].equals("New Biomedical"))
 				{
 					cammsBO.setIsFin07('Y');
 					cammsBO.setIsFin07Migrated('N');
 					cammsBO.setIsFin08b('Y');
 					cammsBO.setIsFin08bMigrated('N');
 				}
 				else if(fieldsArray[20].equals("Purchase Biomedical"))
 				{
 					cammsBO.setIsFin08c('Y');
 					cammsBO.setIsFin08cMigrated('N');
 					cammsBO.setIsFin06('Y');
 					cammsBO.setIsFin06Migrated('N');
 				}	
// 				ErrorCammsBO errorCammsBO =new ErrorCammsBO();
// 				
// 				if(!validateMandatoryCAMMSBO(cammsBO))
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> Mandatory Fields[TCDate||AcceptanceDate||RentalStartDate||WarrantyStartDate] Missing");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}	
// 				if(!compareTCAcceptanceDate(cammsBO))
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> TC is not less than/ equal to Acceptance date");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}
// 				if(!compareTCRentalStartDate(cammsBO))
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> TC is not less than/ equal to Rental Start date");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}
// 				if(!compareAcceptanceRentalStartDate(cammsBO))
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> Acceptance Date is not less than/ equal to Rental Start date");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}
// 				if(!compareAcceptanceWarrantyStartDate(cammsBO))
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> Acceptance Date is not less than/ equal to Warranty Start date");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}
// 				if(differenceStartEndDate(cammsBO.getWarrantyExpiryDate(),cammsBO.getWarrantyStartDate())==12)
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> Warranty End Date should be 12 months from Warranty Start Date");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}	
// 				if(differenceStartEndDate(cammsBO.getRentalEndDate(),cammsBO.getRentalStartDate())==96)
// 				{
// 					errorCammsBO.setCammsbo(cammsBO);
// 					errorCammsBO.setErrorMessage("BENumber::"+cammsBO.getBENumber()+"::> Rental End date should be 96 months from Rental Start Date");
// 					failureCammsBO.add(errorCammsBO);
// 					continue;
// 				}
// 				
 				
 				successCammsBO.add(cammsBO);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		
		System.out.println("size of successCammsBO::> "+ successCammsBO.size());
		//System.out.println("size of failureCammsBO::> "+ failureCammsBO.size());
		
		
//		File outputCSV = new File("Failure.csv");
//		List<String[]> dataLines = new ArrayList<>();
//		
//		for(ErrorCammsBO  errorCammsBO : failureCammsBO)
//		{
//			String line[]= {errorCammsBO.getCammsbo().getBENumber(),errorCammsBO.getErrorMessage()};
//			dataLines.add(line);
//		}
////		String line1[] = {"BENumber1" ,"Error Message1"};
////		String line2[] = {"BENumber2" ,"Error Message2"};
////		dataLines.add(line1);
////		dataLines.add(line2);
//		
//		try( CSVWriter writer = new CSVWriter(new FileWriter("output.csv"));)
//		{
//			writer.writeAll(dataLines);
//			writer.flush();
//		}
//		 catch(IOException e)
//		{
//			 e.printStackTrace();
//		}
////		try(PrintWriter pw= new PrintWriter(outputCSV);)
////		{
////			dataLines.stream().map().map(this::convertToCSV).forEach(pw::println);
////		}
////		catch(IOException e)
////		{}
		
		int persitence =0;
		
		if(successCammsBO.size()>0)
		{
			for(CammsBO camms1BO : successCammsBO)
			{
				try{
					CammsBO temp=	cammsBORepository.findByBENumber(camms1BO.getBENumber());
					if(temp==null)
				{	
					camms1BO.setLastAction("insert");
				cammsBORepository.save(camms1BO);
				}
				else
				{
					camms1BO.setId(temp.getId());
					camms1BO.setLastAction("update");
					cammsBORepository.save(camms1BO);
				}	
				System.out.println("Persistence data:"+ persitence);
				persitence++;
			}
		catch(Exception e)
				{}
			
			//cammsRepository.saveAll(successCammsBO);
		}
		}
		System.out.println("Persistence data:"+ persitence);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	
	public java.sql.Date convertDate(String date)
	{
		java.sql.Date sql = null;
		try
		{
			if(!(date.equals(null)|| date.equals("")))
			
				{System.out.println(date);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			//format.format(date);
		//java.util.Date parsed = sdf.parse(date);
		
			//java.util.Date parsed =sdf2.parse(sdf2.format(sdf.parse(date)));
			java.util.Date parsed  =sdf2.parse(date);
		
		System.out.println(parsed);
	     sql = new java.sql.Date(parsed.getTime());
	     
	     System.out.println(sql);
				}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return sql;
	}
	

	@GetMapping(path = "/{benumber}")
	public CammsBO getCAMMBOByBENumber(@PathVariable(name = "benumber") String benumber) {
		return cammsBORepository.findByBENumber(benumber);
	}
}
