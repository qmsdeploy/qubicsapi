package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.EntityModel.CimsHistoryFin02bEquipment;
import com.example.demo.EntityModel.InvoicePaymentHistory;
import com.example.demo.repository.CimsHistoryFin02bEquipmentRepository;
import com.example.demo.service.Fin02bEquipmentService;


@RestController
@RequestMapping(path="/cims-history-fin-02b-equipment")
@CrossOrigin(origins = "*")
public class CimsHistoryFin02bEquipmentController {
	@Autowired
	private CimsHistoryFin02bEquipmentRepository cimsHistoryFin02bEquipmentRepository;
	
	@Autowired
	private Fin02bEquipmentService fin02bEquipmentService;


	
	@GetMapping(path = "/all")
	public Iterable<CimsHistoryFin02bEquipment> getAllCimsHistoryFin02bEquipment() {
		return cimsHistoryFin02bEquipmentRepository.findAll();
	}
	
	@GetMapping(path = "/{id}")
	public Optional<CimsHistoryFin02bEquipment> getCimsHistoryFin02bEquipmentById(@PathVariable(name = "id") int id) {
		return cimsHistoryFin02bEquipmentRepository.findById(id);
	}
	
	@PostMapping(path = "/add")
	public CimsHistoryFin02bEquipment addCimsHistoryFin02bEquipment(@RequestBody CimsHistoryFin02bEquipment CimsHistoryFin02bEquipment) {
		return cimsHistoryFin02bEquipmentRepository.save(CimsHistoryFin02bEquipment);
	}

	@PutMapping(path = "/update")
	public CimsHistoryFin02bEquipment updateCimsHistoryFin02bEquipment(@RequestBody CimsHistoryFin02bEquipment CimsHistoryFin02bEquipment) {
		return cimsHistoryFin02bEquipmentRepository.save(CimsHistoryFin02bEquipment);
	}

	@DeleteMapping(path = "/delete/{id}")
	public void deleteCimsHistoryFin02bEquipment(@PathVariable(name = "id") int id) {
		cimsHistoryFin02bEquipmentRepository.deleteById(id);
	}
	
//	@PostMapping(path="/equipment-list")
//	public List<CimsHistoryFin02bEquipment> penaltyPaymentToInvoiceSpecial(@RequestBody List<CimsHistoryFin02bEquipment> CimsHistoryFin02bEquipment) {
//		return fin02bEquipmentService.getFin02bEquipmentList(CimsHistoryFin02bEquipment);
//	}
//	
	
@GetMapping(path = "/equipment-list/{stateId},{districtId},{clinicTypeId},{period},{year}")
	public Iterable<CimsHistoryFin02bEquipment>fin02bEquipmentList(@PathVariable(name = "stateId") int stateId, @PathVariable(name = "districtId") int districtId,@PathVariable(name = "clinicTypeId") int clinicTypeId,@PathVariable(name = "period") String period,@PathVariable(name = "year") String year) {
		return fin02bEquipmentService.getFin02bEquipmentList(stateId,districtId,clinicTypeId,period,year);
}
	
	
	
//	
//	@GetMapping(path = "/fin-02b-create-list-older")
//	public Iterable<CimsHistoryFin02b> getFin02bCreateListOlder(@RequestParam(name = "stateId") int stateId, @RequestParam(name = "districtId") int districtId) {
//		return fin02bService.getFin02bCreateListOlder(stateId, districtId);
//	}
//
//	@GetMapping(path = "/districtid-clinictypeid/{districtId},{clinicTypeId},{month},{year}")
//	public CimsHistoryFin02b getAllCimsHistoryFin02bByDistrictIdAndClinicId(@PathVariable(name = "districtId") int districtId,@PathVariable(name = "clinicTypeId") int clinicTypeId,
//			@PathVariable(name = "month") String month,@PathVariable(name = "year") String year) {
//		return cimsHistoryFin02bRepository.findByDistrictIdAndClinicTypeIdAndMonthAndYear(districtId,clinicTypeId,String.valueOf(month),String.valueOf(year));
//	}
//	
//	@GetMapping(path = "/invoice-generation-fin02b-initiate/{month}/{year}/{stateId}/{districtId}")
//	public Iterable<InvoiceGeneration> getInvoiceFin02bInvInitiate(@PathVariable(name = "month") String month,@PathVariable(name = "year") String year,@PathVariable(name = "stateId") Integer stateId,@PathVariable(name = "districtId") Integer districtId) {
//		return fin02bService.getInvoiceFin02bInvInitiate(month,year,stateId,districtId);
//	}
//	@GetMapping(path = "/invoice-generation-fin02b-inprogress/{month}/{year}/{stateId}/{districtId}")
//	public Iterable<InvoiceGeneration> getInvoiceFin02bInvInprogress(@PathVariable(name = "month") String month,@PathVariable(name = "year") String year,@PathVariable(name = "stateId") Integer stateId,@PathVariable(name = "districtId") Integer districtId) {
//		return fin02bService.getInvoiceFin02bInvInprogress(month,year,stateId,districtId);
//	}
////	
//	@GetMapping(path = "/invoice-generation-fin02b-approved/{month}/{year}/{stateId}/{districtId}")
//	public Iterable<InvoiceGeneration> getInvoiceFin02bInvApproved(@PathVariable(name = "month") String month,@PathVariable(name = "year") String year,@PathVariable(name = "stateId") Integer stateId,@PathVariable(name = "districtId") Integer districtId) {
//		return fin02bService.getInvoiceFin02bInvApproved(month,year,stateId,districtId);
//	}
//	
//	
//	@GetMapping(path = "/invoice-generation-fin02b-initiate-older/{month}/{year}/{stateId}/{districtId}")
//	public Iterable<InvoiceGeneration> getInvoiceFin02bInvInitiateOlder(@PathVariable(name = "month") String month,@PathVariable(name = "year") String year,@PathVariable(name = "stateId") Integer stateId,@PathVariable(name = "districtId") Integer districtId) {
//		return fin02bService.getInvoiceFin02bInvInitiateOlder(month,year,stateId,districtId);
//	}
//	@GetMapping(path = "/invoice-generation-fin02b-inprogress-older/{month}/{year}/{stateId}/{districtId}")
//	public Iterable<InvoiceGeneration> getInvoiceFin02bInvInprogressOlder(@PathVariable(name = "month") String month,@PathVariable(name = "year") String year,@PathVariable(name = "stateId") Integer stateId,@PathVariable(name = "districtId") Integer districtId) {
//		return fin02bService.getInvoiceFin02bInvInprogressOlder(month,year,stateId,districtId);
//	}
//	
//	@GetMapping(path = "/invoice-generation-fin02b-approved-older/{month}/{year}/{stateId}/{districtId}")
//	public Iterable<InvoiceGeneration> getInvoiceFin02bInvApprovedOlder(@PathVariable(name = "month") String month,@PathVariable(name = "year") String year,@PathVariable(name = "stateId") Integer stateId,@PathVariable(name = "districtId") Integer districtId) {
//		return fin02bService.getInvoiceFin02bInvApprovedOlder(month,year,stateId,districtId);
//	}
//	
//	
	

}
