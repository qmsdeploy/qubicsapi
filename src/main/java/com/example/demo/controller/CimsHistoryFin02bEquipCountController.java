package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.EntityModel.CimsHistoryFin02bEquipCount;
import com.example.demo.EntityModel.Invoice;
import com.example.demo.repository.CimsHistoryFin02bEquipCountRepository;


@RestController
@RequestMapping(path="/cims-history-fin-02b-equip-count")
@CrossOrigin(origins = "*")
public class CimsHistoryFin02bEquipCountController {
	@Autowired
	private CimsHistoryFin02bEquipCountRepository cimsHistoryFin02bEquipCountRepository;

	
	@GetMapping(path = "/all")
	public Iterable<CimsHistoryFin02bEquipCount> getAllCimsHistoryFin02bEquipCount() {
		return cimsHistoryFin02bEquipCountRepository.findAll();
	}
	
	@GetMapping(path = "/{id}")
	public Optional<CimsHistoryFin02bEquipCount> getCimsHistoryFin02bEquipCountById(@PathVariable(name = "id") int id) {
		return cimsHistoryFin02bEquipCountRepository.findById(id);
	}
	
	@PostMapping(path = "/add")
	public CimsHistoryFin02bEquipCount addCimsHistoryFin02bEquipCount(@RequestBody CimsHistoryFin02bEquipCount CimsHistoryFin02bEquipCount) {
		return cimsHistoryFin02bEquipCountRepository.save(CimsHistoryFin02bEquipCount);
	}

	@PutMapping(path = "/update")
	public CimsHistoryFin02bEquipCount updateCimsHistoryFin02bEquipCount(@RequestBody CimsHistoryFin02bEquipCount CimsHistoryFin02bEquipCount) {
		return cimsHistoryFin02bEquipCountRepository.save(CimsHistoryFin02bEquipCount);
	}

	@DeleteMapping(path = "/delete/{id}")
	public void deleteCimsHistoryFin02bEquipCount(@PathVariable(name = "id") int id) {
		cimsHistoryFin02bEquipCountRepository.deleteById(id);
	}
	
	@GetMapping(path="/equipment-count")
	public Iterable<CimsHistoryFin02bEquipCount> getEquipmentCount(	
			@RequestParam(value = "stateName", required = false) String stateName,
			@RequestParam(value = "districtName", required = false) String districtName,
			@RequestParam(value = "clinicTypeId", required = false) Integer clinicTypeId,
			@RequestParam(value = "period", required = false) String period,
			@RequestParam(value = "year", required = false) String year			
			)  {
		
		return cimsHistoryFin02bEquipCountRepository.findByEquipmentCount(stateName,districtName,clinicTypeId,period,year);
	}

}
