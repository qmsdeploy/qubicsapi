package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.EntityModel.CammsBO;
import com.example.demo.EntityModel.Verify;
import com.example.demo.repository.CammsBORepository;
import com.example.demo.repository.VerifyRepository;

@Component
public class VerifyService {

	@Autowired
	private VerifyRepository verifyRepository;
	
	@Autowired
	private CammsBORepository cammsBORepository;
	
	public List<Verify> verifyData() {
		List<Verify> list =  verifyRepository.findAllVerify();
		List<Verify> failedRecord = new ArrayList<>();
		 list.forEach(verify->{
			 try {
				CammsBO savedDB = cammsBORepository.findByBENumber(verify.getBENumber());
				verify.setIsdataprocessed("Y");
				 if(savedDB!=null) {
					// System.out.println("Data already not saved"+verify.getBENumber()+", verify.getBEConditionalStatus(): "+verify.getBEConditionalStatus() + ", savedDB.getBEConditionalStatus(): "+savedDB.getBEConditionalStatus());
					  if(!verify.getBEConditionalStatus().equals(savedDB.getBEConditionalStatus())) {
						  System.out.println("this is important "+verify.getBENumber());
						 verify.setStatus("No");
						 failedRecord.add(verify);
						 
					 } else if(!verify.getClinicCode().equals(savedDB.getClinicCode())) {
						 verify.setStatus("No");	
						 failedRecord.add(verify);
					 } else if(!verify.getCircle().equals(savedDB.getCircle())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getBatchNumber().equals(savedDB.getBatchNumber())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getCurrentInstallmentNo().equals(savedDB.getCurrentInstallmentNo())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getMonthlyRev().equals(savedDB.getMonthlyRev())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getPurchaseCost().equals(savedDB.getPurchaseCost())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getRentalPerMonth().equals(savedDB.getRentalPerMonth())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getAcceptanceDate().equals(savedDB.getAcceptanceDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getPurchaseDate().equals(savedDB.getPurchaseDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 }  else if(!verify.getAuditStartDate().equals(savedDB.getAuditStartDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getRentalEndDate().equals(savedDB.getRentalEndDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getRentalStartDate().equals(savedDB.getRentalStartDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getWarrantyStartDate().equals(savedDB.getWarrantyStartDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 } else if(!verify.getWarrantyExpiryDate().equals(savedDB.getWarrantyExpiryDate())) {
						 verify.setStatus("No");
						 failedRecord.add(verify);
					 }
				 }
			} catch (Exception e) {
				 System.out.println("Error: Data already not saved"+verify.getBENumber());
			}
		 });
		 verifyRepository.saveAll(list);
		return failedRecord;
	}
}
