package com.example.demo.service;
import java.util.ArrayList;
import org.springframework.data.util.Pair;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.EntityModel.CimsHistoryFin01;
import com.example.demo.EntityModel.CimsHistoryFin02bEquipment;
import com.example.demo.EntityModel.Fin09;
import com.example.demo.EntityModel.Invoice;
import com.example.demo.EntityModel.InvoicePaymentHistory;
import com.example.demo.EntityModel.NumberingSequence;
import com.example.demo.repository.CimsHistoryFin02bEquipmentRepository;

@Service
public class Fin02bEquipmentService {

@Autowired
private CimsHistoryFin02bEquipmentRepository cimsHistoryFin02bEquipmentRepository;

//public List<CimsHistoryFin02bEquipment> getFin02bEquipmentList(List<CimsHistoryFin02bEquipment> cimsHistoryFin02bEquipment){
//	LocalDate date = LocalDate.now();
//	List<CimsHistoryFin02bEquipment> equipmentList = new ArrayList<CimsHistoryFin02bEquipment>();
//	cimsHistoryFin02bEquipment.forEach(cimsHistoryFin02bEquipments -> {
//		if(cimsHistoryFin02bEquipments.getStatus() =="ACTIVE") {
//			cimsHistoryFin02bEquipments.setNumberOfSa7 = 1;
//	});
//	return cimsHistoryFin02bEquipment;
//}

public List<CimsHistoryFin02bEquipment> getFin02bEquipmentList(int stateId, int districtId, int clinicTypeId, String period, String year){
	LocalDate date = LocalDate.now();
	List<String> periods = new ArrayList<String>();
	if(period.equals("P1")) {
		periods.add("P1");
	} else if(period.equals("P2")) {
		periods.add("P1");
		periods.add("P2");
	} else if(period.equals("P3")) {
		periods.add("P1");
		periods.add("P2");
		periods.add("P3");
	}
	List<CimsHistoryFin02bEquipment> equipmentList =  new ArrayList<CimsHistoryFin02bEquipment>();
	List<CimsHistoryFin02bEquipment> equipmentList1 =  new ArrayList<CimsHistoryFin02bEquipment>();
	
	if(stateId == 0 && districtId == 0) {
		equipmentList = cimsHistoryFin02bEquipmentRepository.findByClinicTypeAndPeriodAndYear(clinicTypeId,period,year);
	} else if(stateId != 0 && districtId == 0) {
		equipmentList = cimsHistoryFin02bEquipmentRepository.findByStateAndClinicTypeAndPeriodAndYear(stateId, clinicTypeId,period,year);
	} else if(stateId != 0 && districtId != 0) {
		equipmentList = cimsHistoryFin02bEquipmentRepository.findByStateAndDistrictAndClinicTypeAndYear(stateId,districtId,clinicTypeId,year);
		equipmentList1 = cimsHistoryFin02bEquipmentRepository.findByStateAndDistrictAndClinicTypeAndPeriodInAndYear(stateId,districtId,clinicTypeId,periods,year);
	}
	
	equipmentList.addAll(equipmentList1);
	
	//&& cimsHistoryFin02bEquipment.getPeriod().equals(period) && (cimsHistoryFin02bEquipment.getBEConditionalStatus().equals("ACT") || cimsHistoryFin02bEquipment.getBEConditionalStatus().equals("PBR")) && (cimsHistoryFin02bEquipment.getStatus().equals("ACTIVE")  
   // && Integer.parseInt(cimsHistoryFin02bEquipment.getYear()) <= Integer.parseInt(year)))
	
	equipmentList.forEach(cimsHistoryFin02bEquipment -> {
	    if((cimsHistoryFin02bEquipment.getBEConditionalStatus().equals("ACT") || cimsHistoryFin02bEquipment.getBEConditionalStatus().equals("PBR")) &&(cimsHistoryFin02bEquipment.getStatus().equals("ACTIVE") 
	    		&& cimsHistoryFin02bEquipment.getPeriod().equals(period) && cimsHistoryFin02bEquipment.getYear().equals(year))){
	    	System.out.println("1-"+cimsHistoryFin02bEquipment.getBEConditionalStatus());
	    	System.out.println("2-"+cimsHistoryFin02bEquipment.getStatus());
	    	System.out.println("3-"+cimsHistoryFin02bEquipment.getPeriod());
 	    	System.out.println("4-"+period);
 	    	System.out.println("5-"+cimsHistoryFin02bEquipment.getYear());
    	    System.out.println("6-"+year);
	    	cimsHistoryFin02bEquipment.getBENumber();
	    	cimsHistoryFin02bEquipment.getAssetName();
	    	cimsHistoryFin02bEquipment.getAuditStartDate();
	    	cimsHistoryFin02bEquipment.setPeriodStatus("AA");
	     }else if((cimsHistoryFin02bEquipment.getBEConditionalStatus()!=("ACT") || cimsHistoryFin02bEquipment.getBEConditionalStatus()!=("PBR")) && (cimsHistoryFin02bEquipment.getStatus().equals("INACTIVE")
	    		 && cimsHistoryFin02bEquipment.getPeriod().equals(period) && cimsHistoryFin02bEquipment.getYear().equals(year))){
	    	 System.out.println("11-"+cimsHistoryFin02bEquipment.getBEConditionalStatus());
		    	System.out.println("21-"+cimsHistoryFin02bEquipment.getStatus());
		    	System.out.println("31-"+cimsHistoryFin02bEquipment.getPeriod());
	 	    	System.out.println("41-"+period);
	 	    	System.out.println("51-"+cimsHistoryFin02bEquipment.getYear());
	    	    System.out.println("61-"+year);
	    	 cimsHistoryFin02bEquipment.getBENumber();
		     cimsHistoryFin02bEquipment.getAssetName();
		     cimsHistoryFin02bEquipment.getAuditStartDate();
		     cimsHistoryFin02bEquipment.setPeriodStatus("RD");
	     } else if ((cimsHistoryFin02bEquipment.getBEConditionalStatus().equals("ACT") || cimsHistoryFin02bEquipment.getBEConditionalStatus().equals("PBR")) && (cimsHistoryFin02bEquipment.getStatus().equals("ACTIVE")  
	    		    && Integer.parseInt(cimsHistoryFin02bEquipment.getYear()) <= Integer.parseInt(year))) { 
// 	    	 System.out.println("5-"+cimsHistoryFin02bEquipment.getPeriod());
//     	     System.out.println("6-"+period);
// 	    	 System.out.println("7-"+cimsHistoryFin02bEquipment.getYear());
// 	    	      System.out.println("8-"+year);
	    	     cimsHistoryFin02bEquipment.getBENumber();
			     cimsHistoryFin02bEquipment.getAssetName();
			     cimsHistoryFin02bEquipment.getAuditStartDate();
			     cimsHistoryFin02bEquipment.setPeriodStatus("EE");
			    
		     } 
	     
	    
	});
	
	return equipmentList;
}

}




