package com.example.demo.EntityModel;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;


@Entity
@Table(name="cims_history_fin02b_equip_count")
public class CimsHistoryFin02bEquipCount implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="m_state_id")
	private Integer stateId;
		
	@Formula("(select state.state_name from m_state state where state.id=m_state_id )")
	private String stateName;
	
	@Formula("(select state.state_code from m_state state where state.id=m_state_id )")
	private String stateCode;
	
	@Column(name="m_district_id")
	private Integer districtId;
	
	@Formula("(select district.district_name from m_district district where district.id=m_district_id )")
	private String districtName;
	
	@Formula("(select district.district_code from m_district district where district.id=m_district_id )")
	private String districtCode;
	
	@Formula("(select district.m_circle_id from m_district district where district.id=m_district_id )")
	private Integer circleId;
	
	@Formula("(select circle.circle_name from m_circle circle where circle.id=(select district.m_circle_id from m_district district where district.id=m_district_id ) )")
	private String circleName;
	
	@Column(name="m_clinic_type_id")
	private Integer clinicTypeId;
	
	@Formula("(select clinictype.clinic_type_code from m_clinic_type clinictype where clinictype.id=m_clinic_type_id )")
	private String clinicTypeCode;

	private String month;
	private String year;
	private String period;
	@CreationTimestamp
	private LocalDate date;
	@Column(name="numberof_existing_be")
	private Integer numberofExistingBE;
	@Column(name="ebe_value")
	private Double ebeValue;
	@Column(name="number_of_sa7")
	private Integer numberOfSa7;
	@Column(name="sa7value")
	private Double sa7Value;
	@Column(name="no_of_inactive_existing_be")
	private Integer noOfInactiveExistingBE ;
	@Column(name="inactiv_be_value")
	private Double inactivBEValue ;
	@Column(name="total_ebe")
	private Integer totalEbe;
	@Column(name="total_ebe_value")
	private Double totalEbeValue;
	private String remarks;
	private String createdBy;
	@CreationTimestamp
	private LocalDateTime createdDate;
	private String updatedBy;
	@UpdateTimestamp
	private LocalDateTime updatedDate;
	public CimsHistoryFin02bEquipCount() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "CimsHistoryFin02bEquipCount [id=" + id + ", stateId=" + stateId + ", stateName=" + stateName
				+ ", stateCode=" + stateCode + ", districtId=" + districtId + ", districtName=" + districtName
				+ ", districtCode=" + districtCode + ", circleId=" + circleId + ", circleName=" + circleName
				+ ", clinicTypeId=" + clinicTypeId + ", clinicTypeCode=" + clinicTypeCode + ", month=" + month
				+ ", year=" + year + ", period=" + period + ", date=" + date + ", numberofExistingBE="
				+ numberofExistingBE + ", ebeValue=" + ebeValue + ", numberOfSa7=" + numberOfSa7 + ", sa7Value="
				+ sa7Value + ", noOfInactiveExistingBE=" + noOfInactiveExistingBE + ", inactivBEValue=" + inactivBEValue
				+ ", totalEbe=" + totalEbe + ", totalEbeValue=" + totalEbeValue + ", remarks=" + remarks
				+ ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + "]";
	}
	public CimsHistoryFin02bEquipCount(Integer id, Integer stateId, String stateName, String stateCode,
			Integer districtId, String districtName, String districtCode, Integer circleId, String circleName,
			Integer clinicTypeId, String clinicTypeCode, String month, String year, String period, LocalDate date,
			Integer numberofExistingBE, Double ebeValue, Integer numberOfSa7, Double sa7Value,
			Integer noOfInactiveExistingBE, Double inactivBEValue, Integer totalEbe, Double totalEbeValue,
			String remarks, String createdBy, LocalDateTime createdDate, String updatedBy, LocalDateTime updatedDate) {
		super();
		this.id = id;
		this.stateId = stateId;
		this.stateName = stateName;
		this.stateCode = stateCode;
		this.districtId = districtId;
		this.districtName = districtName;
		this.districtCode = districtCode;
		this.circleId = circleId;
		this.circleName = circleName;
		this.clinicTypeId = clinicTypeId;
		this.clinicTypeCode = clinicTypeCode;
		this.month = month;
		this.year = year;
		this.period = period;
		this.date = date;
		this.numberofExistingBE = numberofExistingBE;
		this.ebeValue = ebeValue;
		this.numberOfSa7 = numberOfSa7;
		this.sa7Value = sa7Value;
		this.noOfInactiveExistingBE = noOfInactiveExistingBE;
		this.inactivBEValue = inactivBEValue;
		this.totalEbe = totalEbe;
		this.totalEbeValue = totalEbeValue;
		this.remarks = remarks;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	
	
	
	
	public CimsHistoryFin02bEquipCount(Integer id, Integer stateId, String stateName, Integer districtId,
			String districtName, Integer clinicTypeId, String clinicTypeCode, String year, String period,
			Integer numberofExistingBE, Integer numberOfSa7, Integer noOfInactiveExistingBE, Integer totalEbe) {
		super();
		this.id = id;
		this.stateId = stateId;
		this.stateName = stateName;
		this.districtId = districtId;
		this.districtName = districtName;
		this.clinicTypeId = clinicTypeId;
		this.clinicTypeCode = clinicTypeCode;
		this.year = year;
		this.period = period;
		this.numberofExistingBE = numberofExistingBE;
		this.numberOfSa7 = numberOfSa7;
		this.noOfInactiveExistingBE = noOfInactiveExistingBE;
		this.totalEbe = totalEbe;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public Integer getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public Integer getCircleId() {
		return circleId;
	}
	public void setCircleId(Integer circleId) {
		this.circleId = circleId;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public Integer getClinicTypeId() {
		return clinicTypeId;
	}
	public void setClinicTypeId(Integer clinicTypeId) {
		this.clinicTypeId = clinicTypeId;
	}
	public String getClinicTypeCode() {
		return clinicTypeCode;
	}
	public void setClinicTypeCode(String clinicTypeCode) {
		this.clinicTypeCode = clinicTypeCode;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Integer getNumberofExistingBE() {
		return numberofExistingBE;
	}
	public void setNumberofExistingBE(Integer numberofExistingBE) {
		this.numberofExistingBE = numberofExistingBE;
	}
	public Double getEbeValue() {
		return ebeValue;
	}
	public void setEbeValue(Double ebeValue) {
		this.ebeValue = ebeValue;
	}
	public Integer getNumberOfSa7() {
		return numberOfSa7;
	}
	public void setNumberOfSa7(Integer numberOfSa7) {
		this.numberOfSa7 = numberOfSa7;
	}
	public Double getSa7Value() {
		return sa7Value;
	}
	public void setSa7Value(Double sa7Value) {
		this.sa7Value = sa7Value;
	}
	public Integer getNoOfInactiveExistingBE() {
		return noOfInactiveExistingBE;
	}
	public void setNoOfInactiveExistingBE(Integer noOfInactiveExistingBE) {
		this.noOfInactiveExistingBE = noOfInactiveExistingBE;
	}
	public Double getInactivBEValue() {
		return inactivBEValue;
	}
	public void setInactivBEValue(Double inactivBEValue) {
		this.inactivBEValue = inactivBEValue;
	}
	public Integer getTotalEbe() {
		return totalEbe;
	}
	public void setTotalEbe(Integer totalEbe) {
		this.totalEbe = totalEbe;
	}
	public Double getTotalEbeValue() {
		return totalEbeValue;
	}
	public void setTotalEbeValue(Double totalEbeValue) {
		this.totalEbeValue = totalEbeValue;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
		 
	
}





