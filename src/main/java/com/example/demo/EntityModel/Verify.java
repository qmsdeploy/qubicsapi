package com.example.demo.EntityModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="verifycammsbo")
public class Verify {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", unique = true, nullable = false)
	private int id;
	@Column(name="acceptance_date")
	private String AcceptanceDate;
	@Column(name="audit_start_date")
	private String AuditStartDate;
	@Column(name="becategory")
	private String BECategory;
	@Column(name="benumber")
    private String BENumber;
	@Column(name="beconditional_status")
    private String BEConditionalStatus;
	@Column(name="batch_number")
    private String BatchNumber;
	@Column(name="circle")
    private String Circle;
	@Column(name="clinic_category")
    private String ClinicCategory;
	@Column(name="clinic_code")
    private String ClinicCode;
	@Column (name="clinic_type")
    private String ClinicType;
	@Column(name="current_installment_no")
    private String CurrentInstallmentNo;
	@Column(name="district")
    private String District;
	@Column(name="installation_date")
    private String InstallationDate;
	@Column(name="model_number")
    private String ModelNumber;
	@Column(name="monthly_rev")
    private String MonthlyRev;
	@Column(name="ownership")
    private String ownership;
	@Column(name="purchase_cost")
    private String PurchaseCost;
	@Column(name="purchase_date")
    private String PurchaseDate;
	@Column(name="rental_end_date")
    private String RentalEndDate;
	@Column(name="rental_per_month")
    private String RentalPerMonth;
	@Column(name="rental_start_date")
    private String RentalStartDate;
	@Column(name="serial_number")
    private String SerialNumber;
	@Column(name="state")
    private String State;
	@Column(name="tcdate")
    private String TCDate;
	@Column(name="warranty_expiry_date")
    private String WarrantyExpiryDate;
	@Column(name="warranty_start_date")
    private String WarrantyStartDate;
	@Column(name="zone")
    private String Zone;
	@Column(name="fin_category")
    private String FinCategory;
	@Column(name="is_migrated")
    private String IsMigrated;
	@Column(name="action_type")
    private String ActionType;
	@Column(name="status")
    private String Status;
	@Column(name="is_data_processed")
    private String Isdataprocessed;
	public Verify() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Verify(int id, String acceptanceDate, String auditStartDate, String bECategory, String bENumber,
			String bEConditionalStatus, String batchNumber, String circle, String clinicCategory, String clinicCode,
			String clinicType, String currentInstallmentNo, String district, String installationDate,
			String modelNumber, String monthlyRev, String ownership, String purchaseCost, String purchaseDate,
			String rentalEndDate, String rentalPerMonth, String rentalStartDate, String serialNumber, String state,
			String tCDate, String warrantyExpiryDate, String warrantyStartDate, String zone, String finCategory,
			String isMigrated, String actionType, String status, String isdataprocessed) {
		super();
		this.id = id;
		AcceptanceDate = acceptanceDate;
		AuditStartDate = auditStartDate;
		BECategory = bECategory;
		BENumber = bENumber;
		BEConditionalStatus = bEConditionalStatus;
		BatchNumber = batchNumber;
		Circle = circle;
		ClinicCategory = clinicCategory;
		ClinicCode = clinicCode;
		ClinicType = clinicType;
		CurrentInstallmentNo = currentInstallmentNo;
		District = district;
		InstallationDate = installationDate;
		ModelNumber = modelNumber;
		MonthlyRev = monthlyRev;
		this.ownership = ownership;
		PurchaseCost = purchaseCost;
		PurchaseDate = purchaseDate;
		RentalEndDate = rentalEndDate;
		RentalPerMonth = rentalPerMonth;
		RentalStartDate = rentalStartDate;
		SerialNumber = serialNumber;
		State = state;
		TCDate = tCDate;
		WarrantyExpiryDate = warrantyExpiryDate;
		WarrantyStartDate = warrantyStartDate;
		Zone = zone;
		FinCategory = finCategory;
		IsMigrated = isMigrated;
		ActionType = actionType;
		Status = status;
		Isdataprocessed = isdataprocessed;
	}
	@Override
	public String toString() {
		return "Verify [id=" + id + ", AcceptanceDate=" + AcceptanceDate + ", AuditStartDate=" + AuditStartDate
				+ ", BECategory=" + BECategory + ", BENumber=" + BENumber + ", BEConditionalStatus="
				+ BEConditionalStatus + ", BatchNumber=" + BatchNumber + ", Circle=" + Circle + ", ClinicCategory="
				+ ClinicCategory + ", ClinicCode=" + ClinicCode + ", ClinicType=" + ClinicType
				+ ", CurrentInstallmentNo=" + CurrentInstallmentNo + ", District=" + District + ", InstallationDate="
				+ InstallationDate + ", ModelNumber=" + ModelNumber + ", MonthlyRev=" + MonthlyRev + ", ownership="
				+ ownership + ", PurchaseCost=" + PurchaseCost + ", PurchaseDate=" + PurchaseDate + ", RentalEndDate="
				+ RentalEndDate + ", RentalPerMonth=" + RentalPerMonth + ", RentalStartDate=" + RentalStartDate
				+ ", SerialNumber=" + SerialNumber + ", State=" + State + ", TCDate=" + TCDate + ", WarrantyExpiryDate="
				+ WarrantyExpiryDate + ", WarrantyStartDate=" + WarrantyStartDate + ", Zone=" + Zone + ", FinCategory="
				+ FinCategory + ", IsMigrated=" + IsMigrated + ", ActionType=" + ActionType + ", Status=" + Status
				+ ", Isdataprocessed=" + Isdataprocessed + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAcceptanceDate() {
		return AcceptanceDate;
	}
	public void setAcceptanceDate(String acceptanceDate) {
		AcceptanceDate = acceptanceDate;
	}
	public String getAuditStartDate() {
		return AuditStartDate;
	}
	public void setAuditStartDate(String auditStartDate) {
		AuditStartDate = auditStartDate;
	}
	public String getBECategory() {
		return BECategory;
	}
	public void setBECategory(String bECategory) {
		BECategory = bECategory;
	}
	public String getBENumber() {
		return BENumber;
	}
	public void setBENumber(String bENumber) {
		BENumber = bENumber;
	}
	public String getBEConditionalStatus() {
		return BEConditionalStatus;
	}
	public void setBEConditionalStatus(String bEConditionalStatus) {
		BEConditionalStatus = bEConditionalStatus;
	}
	public String getBatchNumber() {
		return BatchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		BatchNumber = batchNumber;
	}
	public String getCircle() {
		return Circle;
	}
	public void setCircle(String circle) {
		Circle = circle;
	}
	public String getClinicCategory() {
		return ClinicCategory;
	}
	public void setClinicCategory(String clinicCategory) {
		ClinicCategory = clinicCategory;
	}
	public String getClinicCode() {
		return ClinicCode;
	}
	public void setClinicCode(String clinicCode) {
		ClinicCode = clinicCode;
	}
	public String getClinicType() {
		return ClinicType;
	}
	public void setClinicType(String clinicType) {
		ClinicType = clinicType;
	}
	public String getCurrentInstallmentNo() {
		return CurrentInstallmentNo;
	}
	public void setCurrentInstallmentNo(String currentInstallmentNo) {
		CurrentInstallmentNo = currentInstallmentNo;
	}
	public String getDistrict() {
		return District;
	}
	public void setDistrict(String district) {
		District = district;
	}
	public String getInstallationDate() {
		return InstallationDate;
	}
	public void setInstallationDate(String installationDate) {
		InstallationDate = installationDate;
	}
	public String getModelNumber() {
		return ModelNumber;
	}
	public void setModelNumber(String modelNumber) {
		ModelNumber = modelNumber;
	}
	public String getMonthlyRev() {
		return MonthlyRev;
	}
	public void setMonthlyRev(String monthlyRev) {
		MonthlyRev = monthlyRev;
	}
	public String getOwnership() {
		return ownership;
	}
	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}
	public String getPurchaseCost() {
		return PurchaseCost;
	}
	public void setPurchaseCost(String purchaseCost) {
		PurchaseCost = purchaseCost;
	}
	public String getPurchaseDate() {
		return PurchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		PurchaseDate = purchaseDate;
	}
	public String getRentalEndDate() {
		return RentalEndDate;
	}
	public void setRentalEndDate(String rentalEndDate) {
		RentalEndDate = rentalEndDate;
	}
	public String getRentalPerMonth() {
		return RentalPerMonth;
	}
	public void setRentalPerMonth(String rentalPerMonth) {
		RentalPerMonth = rentalPerMonth;
	}
	public String getRentalStartDate() {
		return RentalStartDate;
	}
	public void setRentalStartDate(String rentalStartDate) {
		RentalStartDate = rentalStartDate;
	}
	public String getSerialNumber() {
		return SerialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		SerialNumber = serialNumber;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getTCDate() {
		return TCDate;
	}
	public void setTCDate(String tCDate) {
		TCDate = tCDate;
	}
	public String getWarrantyExpiryDate() {
		return WarrantyExpiryDate;
	}
	public void setWarrantyExpiryDate(String warrantyExpiryDate) {
		WarrantyExpiryDate = warrantyExpiryDate;
	}
	public String getWarrantyStartDate() {
		return WarrantyStartDate;
	}
	public void setWarrantyStartDate(String warrantyStartDate) {
		WarrantyStartDate = warrantyStartDate;
	}
	public String getZone() {
		return Zone;
	}
	public void setZone(String zone) {
		Zone = zone;
	}
	public String getFinCategory() {
		return FinCategory;
	}
	public void setFinCategory(String finCategory) {
		FinCategory = finCategory;
	}
	public String getIsMigrated() {
		return IsMigrated;
	}
	public void setIsMigrated(String isMigrated) {
		IsMigrated = isMigrated;
	}
	public String getActionType() {
		return ActionType;
	}
	public void setActionType(String actionType) {
		ActionType = actionType;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getIsdataprocessed() {
		return Isdataprocessed;
	}
	public void setIsdataprocessed(String isdataprocessed) {
		Isdataprocessed = isdataprocessed;
	}
	
	
}
