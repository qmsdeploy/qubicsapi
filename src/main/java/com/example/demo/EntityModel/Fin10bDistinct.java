package com.example.demo.EntityModel;
//import com.example.demo.EntityModel.Fin10b;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Fin10bDistinct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer stateId;
	private Integer districtId;
	private Integer clinicTypeId;
	private String status;
	private Integer clinicId;
	private Double totalAmount;

//	private String month;
//	private String year;
//	
	
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public Double getTotalAmount() {
		return totalAmount;
	}



	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}



	public Fin10bDistinct(Integer id,Integer stateId, Integer districtId,
			 String status, Integer clinicId,Double totalAmount, Integer clinicTypeId) {
		super();
		this.id = id;
		this.stateId = stateId;
		this.districtId = districtId;
		this.clinicTypeId = clinicTypeId;
		this.status = status;
		this.clinicId = clinicId;
		this.totalAmount = totalAmount;
//		this.month = month;
//		this.year = year;
	}

	 

	public Integer getStateId() {
		return stateId;
	}



	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}



	public Integer getDistrictId() {
		return districtId;
	}



	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}



	public Integer getClinicTypeId() {
		return clinicTypeId;
	}



	public void setClinicTypeId(Integer clinicTypeId) {
		this.clinicTypeId = clinicTypeId;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}


//
//	public String getMonth() {
//		return month;
//	}
//
//
//
//	public void setMonth(String month) {
//		this.month = month;
//	}
//
//
//
//	public String getYear() {
//		return year;
//	}
//
//
//
//	public void setYear(String year) {
//		this.year = year;
//	}

	 public Integer getclinicId() {
			return clinicId;
		}



		public void setclinicId(Integer clinicId) {
			this.clinicId = clinicId;
		}



//		@Override
//		public int hashCode() {
//			final int prime = 31;
//			int result = 1;
//			result = prime * result + ((clinicId == null) ? 0 : clinicId.hashCode());
//			return result;
//		}
//
//		public Integer sample (Fin10b Fin10b) {
//			return com.example.demo.EntityModel.Fin10b.getclinicId();
//		}
//
//		@Override
//		public boolean equals(Object obj) {
//			
//			Fin10bDistinct other = (Fin10bDistinct) obj;
//			if (clinicId == Fin10b.getclinicId()) {
//				if (other.clinicId != Fin10b.getclinicId())
//					return false;
//			}
//			else if (!Fin10b.getclinicId().equals(other.clinicId))
//				return false;
//			return true;
//		}



		

	@Override
	   public int hashCode() {
	      return id;
	   }


	@Override
	   public boolean equals(Object obj) {
		if (obj instanceof Fin10bDistinct) {
			Fin10bDistinct cm = (Fin10bDistinct) obj;
			return (cm.id.equals(this.id) ); 
//			return (cm.districtId.equals(this.districtId) && cm.clinicId.equals(this.clinicId) && cm.totalAmount.equals(this.totalAmount) && cm.clinicTypeId.equals(this.clinicTypeId) ); 

//			return (cm.districtId.equals(this.districtId) && cm.clinicTypeId.equals(this.clinicTypeId) && cm.month.equals(this.month)&& cm.year.equals(this.year) ); 

			
		} else {
			return false;
		}

	   }
	

}


