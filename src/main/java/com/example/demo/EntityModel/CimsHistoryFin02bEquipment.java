package com.example.demo.EntityModel;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;


@Entity
@Table(name="cims_history_fin02b_equipment")
public class CimsHistoryFin02bEquipment implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="m_state_id")
	private Integer stateId;
	
	@Formula("(select state.state_name from m_state state where state.id=m_state_id )")
	private String stateName;
	
	@Formula("(select state.state_code from m_state state where state.id=m_state_id )")
	private String stateCode;
	
	@Column(name="m_district_id")
	private Integer districtId;
	
	@Formula("(select district.district_name from m_district district where district.id=m_district_id )")
	private String districtName;
	
	@Formula("(select district.district_code from m_district district where district.id=m_district_id )")
	private String districtCode;
	
	@Formula("(select district.m_circle_id from m_district district where district.id=m_district_id )")
	private Integer circleId;
	
	@Formula("(select circle.circle_name from m_circle circle where circle.id=(select district.m_circle_id from m_district district where district.id=m_district_id ) )")
	private String circleName;
	
	@Column(name="m_clinic_type_id")
	private Integer clinicTypeId;
	
	@Formula("(select clinictype.clinic_type_code from m_clinic_type clinictype where clinictype.id=m_clinic_type_id )")
	private String clinicTypeCode;
	
	@Column(name="m_clinic_id")
	private String clinicId;
	
	@Formula("(select clinic.clinic_name from m_clinic clinic where clinic.id=m_clinic_id )")
	private String clinicName;

	@Column(name="be_number")
	private String BENumber;
	private String assetName ;
	private String month;
	private String year;
	private String period;
	@CreationTimestamp
	private LocalDate date;
	@Column(name="numberof_existing_be")
	private Integer numberofExistingBE;
	private Double ebeValue;
	private Integer numberOfSa7;
	private Double sa7Value;
	private Integer totalEbe;
	private Double totalEbeValue;
	private String remarks;
	private String status ;
	private String BEConditionalStatus;
	private Date auditStartDate;
	private String createdBy;
	@CreationTimestamp
	private LocalDateTime createdDate;
	private String updatedBy;
	@UpdateTimestamp
	private LocalDateTime updatedDate;
	private String periodStatus;
	
	public CimsHistoryFin02bEquipment() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "CimsHistoryFin02bEquipment [id=" + id + ", stateId=" + stateId + ", stateName=" + stateName
				+ ", stateCode=" + stateCode + ", districtId=" + districtId + ", districtName=" + districtName
				+ ", districtCode=" + districtCode + ", circleId=" + circleId + ", circleName=" + circleName
				+ ", clinicTypeId=" + clinicTypeId + ", clinicTypeCode=" + clinicTypeCode + ", clinicId=" + clinicId
				+ ", clinicName=" + clinicName + ", BENumber=" + BENumber + ", assetName=" + assetName + ", month="
				+ month + ", year=" + year + ", period=" + period + ", date=" + date + ", numberofExistingBE="
				+ numberofExistingBE + ", ebeValue=" + ebeValue + ", numberOfSa7=" + numberOfSa7 + ", sa7Value="
				+ sa7Value + ", totalEbe=" + totalEbe + ", totalEbeValue=" + totalEbeValue + ", remarks=" + remarks
				+ ", status=" + status + ", BEConditionalStatus=" + BEConditionalStatus + ", auditStartDate="
				+ auditStartDate + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy="
				+ updatedBy + ", updatedDate=" + updatedDate + ", periodStatus=" + periodStatus + "]";
	}

	public CimsHistoryFin02bEquipment(Integer id, Integer stateId, String stateName, String stateCode,
			Integer districtId, String districtName, String districtCode, Integer circleId, String circleName,
			Integer clinicTypeId, String clinicTypeCode, String clinicId, String clinicName, String bENumber,
			String assetName, String month, String year, String period, LocalDate date, Integer numberofExistingBE,
			Double ebeValue, Integer numberOfSa7, Double sa7Value, Integer totalEbe, Double totalEbeValue,
			String remarks, String status, String bEConditionalStatus, Date auditStartDate, String createdBy,
			LocalDateTime createdDate, String updatedBy, LocalDateTime updatedDate, String periodStatus) {
		super();
		this.id = id;
		this.stateId = stateId;
		this.stateName = stateName;
		this.stateCode = stateCode;
		this.districtId = districtId;
		this.districtName = districtName;
		this.districtCode = districtCode;
		this.circleId = circleId;
		this.circleName = circleName;
		this.clinicTypeId = clinicTypeId;
		this.clinicTypeCode = clinicTypeCode;
		this.clinicId = clinicId;
		this.clinicName = clinicName;
		BENumber = bENumber;
		this.assetName = assetName;
		this.month = month;
		this.year = year;
		this.period = period;
		this.date = date;
		this.numberofExistingBE = numberofExistingBE;
		this.ebeValue = ebeValue;
		this.numberOfSa7 = numberOfSa7;
		this.sa7Value = sa7Value;
		this.totalEbe = totalEbe;
		this.totalEbeValue = totalEbeValue;
		this.remarks = remarks;
		this.status = status;
		BEConditionalStatus = bEConditionalStatus;
		this.auditStartDate = auditStartDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.periodStatus = periodStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public Integer getCircleId() {
		return circleId;
	}

	public void setCircleId(Integer circleId) {
		this.circleId = circleId;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public Integer getClinicTypeId() {
		return clinicTypeId;
	}

	public void setClinicTypeId(Integer clinicTypeId) {
		this.clinicTypeId = clinicTypeId;
	}

	public String getClinicTypeCode() {
		return clinicTypeCode;
	}

	public void setClinicTypeCode(String clinicTypeCode) {
		this.clinicTypeCode = clinicTypeCode;
	}

	public String getClinicId() {
		return clinicId;
	}

	public void setClinicId(String clinicId) {
		this.clinicId = clinicId;
	}

	public String getClinicName() {
		return clinicName;
	}

	public void setClinicName(String clinicName) {
		this.clinicName = clinicName;
	}

	public String getBENumber() {
		return BENumber;
	}

	public void setBENumber(String bENumber) {
		BENumber = bENumber;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getNumberofExistingBE() {
		return numberofExistingBE;
	}

	public void setNumberofExistingBE(Integer numberofExistingBE) {
		this.numberofExistingBE = numberofExistingBE;
	}

	public Double getEbeValue() {
		return ebeValue;
	}

	public void setEbeValue(Double ebeValue) {
		this.ebeValue = ebeValue;
	}

	public Integer getNumberOfSa7() {
		return numberOfSa7;
	}

	public void setNumberOfSa7(Integer numberOfSa7) {
		this.numberOfSa7 = numberOfSa7;
	}

	public Double getSa7Value() {
		return sa7Value;
	}

	public void setSa7Value(Double sa7Value) {
		this.sa7Value = sa7Value;
	}

	public Integer getTotalEbe() {
		return totalEbe;
	}

	public void setTotalEbe(Integer totalEbe) {
		this.totalEbe = totalEbe;
	}

	public Double getTotalEbeValue() {
		return totalEbeValue;
	}

	public void setTotalEbeValue(Double totalEbeValue) {
		this.totalEbeValue = totalEbeValue;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBEConditionalStatus() {
		return BEConditionalStatus;
	}

	public void setBEConditionalStatus(String bEConditionalStatus) {
		BEConditionalStatus = bEConditionalStatus;
	}

	public Date getAuditStartDate() {
		return auditStartDate;
	}

	public void setAuditStartDate(Date auditStartDate) {
		this.auditStartDate = auditStartDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getPeriodStatus() {
		return periodStatus;
	}

	public void setPeriodStatus(String periodStatus) {
		this.periodStatus = periodStatus;
	}
	 
	
}
