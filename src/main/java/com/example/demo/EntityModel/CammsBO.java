package com.example.demo.EntityModel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
// @Table(name="cammsbo")
@Table(name = "cammsbo_bck_31-1-22")
public class CammsBO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", unique = true, nullable = false)
	private Integer id;
	
    private String BENumber;
    private String BECategory;
    private String ClinicCategory;
    private String Zone;
    private String ClinicCode;
    private String District;
    private String Circle;
    private Date InstallationDate;
    private Date TCDate;
    private Date AcceptanceDate;
    private Date RentalStartDate;
    private Date RentalEndDate;
    private Date WarrantyExpiryDate;
    private Date WarrantyStartDate;
    private Date PurchaseDate;
    private String ClinicType;
    private String State;
    private float MonthlyRev;
    private float RentalPerMonth;
    private String BatchNumber;
    private String ownership;
    private float PurchaseCost;
    private String ModelNumber;
    private String SerialNumber;
    private String CurrentInstallmentNo;
    private String BEConditionalStatus;
    private Date AuditStartDate;
    
   // private String isMigrated;
    private char isFin07;
    private char isFin07Migrated;
    private char isFin08b;
    private char isFin08bMigrated;
    private char isFin08c;
    private char isFin08cMigrated;
    private char isFin06;
    private char isFin06Migrated;
    
    private String createdBy;
    @CreationTimestamp
    private Date createdDate;
   
    private String updateBy;
    @UpdateTimestamp
    private Date updatedDate;
    private String lastAction;
	public CammsBO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CammsBO(Integer id, String bENumber, String bECategory, String clinicCategory, String zone,
			String clinicCode, String district, String circle, Date installationDate, Date tCDate, Date acceptanceDate,
			Date rentalStartDate, Date rentalEndDate, Date warrantyExpiryDate, Date warrantyStartDate,
			Date purchaseDate, String clinicType, String state, float monthlyRev, float rentalPerMonth,
			String batchNumber, String ownership, float purchaseCost, String modelNumber, String serialNumber,
			String currentInstallmentNo, String bEConditionalStatus, Date auditStartDate, char isFin07,
			char isFin07Migrated, char isFin08b, char isFin08bMigrated, char isFin08c, char isFin08cMigrated,
			char isFin06, char isFin06Migrated, String createdBy, Date createdDate, String updateBy, Date updatedDate,
			String lastAction) {
		super();
		this.id = id;
		BENumber = bENumber;
		BECategory = bECategory;
		ClinicCategory = clinicCategory;
		Zone = zone;
		ClinicCode = clinicCode;
		District = district;
		Circle = circle;
		InstallationDate = installationDate;
		TCDate = tCDate;
		AcceptanceDate = acceptanceDate;
		RentalStartDate = rentalStartDate;
		RentalEndDate = rentalEndDate;
		WarrantyExpiryDate = warrantyExpiryDate;
		WarrantyStartDate = warrantyStartDate;
		PurchaseDate = purchaseDate;
		ClinicType = clinicType;
		State = state;
		MonthlyRev = monthlyRev;
		RentalPerMonth = rentalPerMonth;
		BatchNumber = batchNumber;
		this.ownership = ownership;
		PurchaseCost = purchaseCost;
		ModelNumber = modelNumber;
		SerialNumber = serialNumber;
		CurrentInstallmentNo = currentInstallmentNo;
		BEConditionalStatus = bEConditionalStatus;
		AuditStartDate = auditStartDate;
		this.isFin07 = isFin07;
		this.isFin07Migrated = isFin07Migrated;
		this.isFin08b = isFin08b;
		this.isFin08bMigrated = isFin08bMigrated;
		this.isFin08c = isFin08c;
		this.isFin08cMigrated = isFin08cMigrated;
		this.isFin06 = isFin06;
		this.isFin06Migrated = isFin06Migrated;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updateBy = updateBy;
		this.updatedDate = updatedDate;
		this.lastAction = lastAction;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBENumber() {
		return BENumber;
	}
	public void setBENumber(String bENumber) {
		BENumber = bENumber;
	}
	public String getBECategory() {
		return BECategory;
	}
	public void setBECategory(String bECategory) {
		BECategory = bECategory;
	}
	public String getClinicCategory() {
		return ClinicCategory;
	}
	public void setClinicCategory(String clinicCategory) {
		ClinicCategory = clinicCategory;
	}
	public String getZone() {
		return Zone;
	}
	public void setZone(String zone) {
		Zone = zone;
	}
	public String getClinicCode() {
		return ClinicCode;
	}
	public void setClinicCode(String clinicCode) {
		ClinicCode = clinicCode;
	}
	public String getDistrict() {
		return District;
	}
	public void setDistrict(String district) {
		District = district;
	}
	public String getCircle() {
		return Circle;
	}
	public void setCircle(String circle) {
		Circle = circle;
	}
	public Date getInstallationDate() {
		return InstallationDate;
	}
	public void setInstallationDate(Date installationDate) {
		InstallationDate = installationDate;
	}
	public Date getTCDate() {
		return TCDate;
	}
	public void setTCDate(Date tCDate) {
		TCDate = tCDate;
	}
	public Date getAcceptanceDate() {
		return AcceptanceDate;
	}
	public void setAcceptanceDate(Date acceptanceDate) {
		AcceptanceDate = acceptanceDate;
	}
	public Date getRentalStartDate() {
		return RentalStartDate;
	}
	public void setRentalStartDate(Date rentalStartDate) {
		RentalStartDate = rentalStartDate;
	}
	public Date getRentalEndDate() {
		return RentalEndDate;
	}
	public void setRentalEndDate(Date rentalEndDate) {
		RentalEndDate = rentalEndDate;
	}
	public Date getWarrantyExpiryDate() {
		return WarrantyExpiryDate;
	}
	public void setWarrantyExpiryDate(Date warrantyExpiryDate) {
		WarrantyExpiryDate = warrantyExpiryDate;
	}
	public Date getWarrantyStartDate() {
		return WarrantyStartDate;
	}
	public void setWarrantyStartDate(Date warrantyStartDate) {
		WarrantyStartDate = warrantyStartDate;
	}
	public Date getPurchaseDate() {
		return PurchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		PurchaseDate = purchaseDate;
	}
	public String getClinicType() {
		return ClinicType;
	}
	public void setClinicType(String clinicType) {
		ClinicType = clinicType;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public float getMonthlyRev() {
		return MonthlyRev;
	}
	public void setMonthlyRev(float monthlyRev) {
		MonthlyRev = monthlyRev;
	}
	public float getRentalPerMonth() {
		return RentalPerMonth;
	}
	public void setRentalPerMonth(float rentalPerMonth) {
		RentalPerMonth = rentalPerMonth;
	}
	public String getBatchNumber() {
		return BatchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		BatchNumber = batchNumber;
	}
	public String getOwnership() {
		return ownership;
	}
	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}
	public float getPurchaseCost() {
		return PurchaseCost;
	}
	public void setPurchaseCost(float purchaseCost) {
		PurchaseCost = purchaseCost;
	}
	public String getModelNumber() {
		return ModelNumber;
	}
	public void setModelNumber(String modelNumber) {
		ModelNumber = modelNumber;
	}
	public String getSerialNumber() {
		return SerialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		SerialNumber = serialNumber;
	}
	public String getCurrentInstallmentNo() {
		return CurrentInstallmentNo;
	}
	public void setCurrentInstallmentNo(String currentInstallmentNo) {
		CurrentInstallmentNo = currentInstallmentNo;
	}
	public String getBEConditionalStatus() {
		return BEConditionalStatus;
	}
	public void setBEConditionalStatus(String bEConditionalStatus) {
		BEConditionalStatus = bEConditionalStatus;
	}
	public Date getAuditStartDate() {
		return AuditStartDate;
	}
	public void setAuditStartDate(Date auditStartDate) {
		AuditStartDate = auditStartDate;
	}
	public char getIsFin07() {
		return isFin07;
	}
	public void setIsFin07(char isFin07) {
		this.isFin07 = isFin07;
	}
	public char getIsFin07Migrated() {
		return isFin07Migrated;
	}
	public void setIsFin07Migrated(char isFin07Migrated) {
		this.isFin07Migrated = isFin07Migrated;
	}
	public char getIsFin08b() {
		return isFin08b;
	}
	public void setIsFin08b(char isFin08b) {
		this.isFin08b = isFin08b;
	}
	public char getIsFin08bMigrated() {
		return isFin08bMigrated;
	}
	public void setIsFin08bMigrated(char isFin08bMigrated) {
		this.isFin08bMigrated = isFin08bMigrated;
	}
	public char getIsFin08c() {
		return isFin08c;
	}
	public void setIsFin08c(char isFin08c) {
		this.isFin08c = isFin08c;
	}
	public char getIsFin08cMigrated() {
		return isFin08cMigrated;
	}
	public void setIsFin08cMigrated(char isFin08cMigrated) {
		this.isFin08cMigrated = isFin08cMigrated;
	}
	public char getIsFin06() {
		return isFin06;
	}
	public void setIsFin06(char isFin06) {
		this.isFin06 = isFin06;
	}
	public char getIsFin06Migrated() {
		return isFin06Migrated;
	}
	public void setIsFin06Migrated(char isFin06Migrated) {
		this.isFin06Migrated = isFin06Migrated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getLastAction() {
		return lastAction;
	}
	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}
    
	

}
