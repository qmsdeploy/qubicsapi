package com.example.demo.EntityModel.UIEntityModels;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
@Entity
@Table(name="t_invoice")
@Data
public class InvoiceForFin2InProgressList implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	private String code;

	private String invoiceNo;

	@Column(name="m_invoice_type_id")
	private Integer invoiceTypeId;

	@Formula("(select invoicetype.invoice_type_name from m_invoice_type invoicetype  where invoicetype.id=m_invoice_type_id )")
	private String invoiceTypeName;

	@Column(name="m_state_id")
	private Integer stateId;

	@Formula("(select state.state_name from m_state state where state.id=m_state_id )")
	private String stateName;
	
	@Formula("(select state.state_code from m_state state where state.id=m_state_id )")
	private String stateCode;
	
	@Column(name="m_district_id")
	private Integer districtId;
	
	@Formula("(select district.district_name from m_district district where district.id=m_district_id )")
	private String districtName;
	
	@Formula("(select district.district_code from m_district district where district.id=m_district_id )")
	private String districtCode;
	
	@Column(name="m_clinic_type_id")
	private Integer clinicTypeId;
	
	@Formula("(select clinictype.clinic_type_code from m_clinic_type clinictype where clinictype.id=m_clinic_type_id )")
	private String clinicTypeCode;
	
	@Formula("(select mc.circle_code from m_circle mc where mc.id = (SELECT md.m_circle_id from m_district md where md.id  = m_district_id))")
	private String circleCode;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin01_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<Fin06Ui> Fin06;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin02a_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<Fin03aUi> Fin03a;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin02_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<Fin03Ui> Fin03;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin02b_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<CimsHistoryFin02bUi> CimsHistoryFin02b;

	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin04_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<Fin10bUi> Fin10b;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin05a_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<Fin05aSiteConformityUi> Fin05aSiteConformity;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_fin11_inv_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<Fin11ConcessionElementsUi> Fin11ConcessionElements;
	
	// @OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="t_inv_ref_no", referencedColumnName = "invoiceNo",insertable = false, updatable = false)
	// private List<InvoicePaymentHistoryUi> InvoicePaymentHistory;
	
	private String month;
	private String year;
	@CreationTimestamp
	private LocalDate invoiceDate;
	private Double invoiceBaseValue;
	private Double sst;
	private Double netAfterSst; 
	private Double retentionAmount;
	private Double totalInvoiceValue;
	private Double totalInvoiceValueWoRetention;
	private Double outstandingAmount;
	private String status = "APPROVED BY MOH";
	private LocalDate paymentDate;
	private String paymentStatus;
	private String entryType;
	private String quater;
	private String paymentRefNo;
	private Double paymentReceived;
	
	private Double netRetentionAmount;
	private Integer submittedUserId;
	private Integer approval1UserId;
	private Integer approval2UserId;
	
	@Column(name="debit_note_entry")
	private Double debitNoteEntry;

	@Formula("(select u.name from user u where u.id=submitted_user_id )")
	private String submittedUserName;
	
	@Formula("(select u.name from user u where u.id=approval1user_id )")
	private String approval1UserName;
	
	@Formula("(select u.name from user u where u.id=approval2user_id )")
	private String approval2UserName;
	
	@Formula("(select u.designation from user u where u.id=submitted_user_id )")
	private String submittedUserDesignation;
	
	@Formula("(select u.designation from user u where u.id=approval1user_id )")
	private String approval1UserDesignation;
	
	@Formula("(select u.designation from user u where u.id=approval2user_id )")
	private String approval2UserDesignation;

	private LocalDate submittedDate;

	private LocalDate approval1Date;

	private LocalDate approval2Date;
	
	private String remarks;
	private String createdBy;
	@CreationTimestamp
	private LocalDateTime createdDate;
	private String updatedBy;
	@UpdateTimestamp
	private LocalDateTime updatedDate;
	
	

}

