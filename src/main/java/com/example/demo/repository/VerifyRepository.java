package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.EntityModel.Verify;
import com.google.common.base.Optional;

@Repository
public interface VerifyRepository extends CrudRepository <Verify, Integer >{

	Optional<Verify> findByBENumber (String BENumber);
	
	@Query("FROM Verify")
	List<Verify> findAllVerify();
}
