package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.EntityModel.CimsHistoryFin02bEquipCount;



public interface CimsHistoryFin02bEquipCountRepository extends CrudRepository<CimsHistoryFin02bEquipCount, Integer> {

	@Query("select new com.example.demo.EntityModel.CimsHistoryFin02bEquipCount(i.id,i.stateId,i.stateName,"
			+ "i.districtId,i.districtName,i.clinicTypeId,i.clinicTypeCode,i.year,i.period,"
			+ "i.numberofExistingBE,i.numberOfSa7,i.noOfInactiveExistingBE,i.totalEbe) from CimsHistoryFin02bEquipCount i where ifnull(LOWER(i.stateName),'') LIKE LOWER(CONCAT('%',ifnull(:stateName,''),'%')) "
			+ "AND ifnull(LOWER(i.districtName),'') LIKE LOWER(CONCAT('%',ifnull(:districtName,''), '%')) "
			+ "AND ifnull(LOWER(i.clinicTypeId),'') LIKE LOWER(CONCAT('%',ifnull(:clinicTypeId,''), '%')) "
			+ "AND ifnull(LOWER(i.period),'')LIKE LOWER(CONCAT('%',ifnull(:period,''), '%'))"
			+ "AND ifnull(LOWER(i.year),'') LIKE LOWER(CONCAT('%',ifnull(:year,''), '%')) ORDER BY stateId,districtId,clinicTypeId,period,year ASC ")
	List<CimsHistoryFin02bEquipCount> findByEquipmentCount(@Param("stateName") String stateName,@Param("districtName") String districtName ,@Param("clinicTypeId") Integer clinicTypeId,@Param("period") String period , @Param("year") String year);


}
