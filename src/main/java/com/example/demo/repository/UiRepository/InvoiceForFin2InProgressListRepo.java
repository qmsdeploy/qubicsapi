package com.example.demo.repository.UiRepository;

import java.util.List;

import com.example.demo.EntityModel.UIEntityModels.InvoiceForFin2InProgressList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InvoiceForFin2InProgressListRepo extends JpaRepository<InvoiceForFin2InProgressList,Integer> {

    
    @Query("FROM InvoiceForFin2InProgressList WHERE invoiceTypeId = :invoiceTypeId AND status != :status AND status IS NOT NULL AND status !='' AND month = :month AND year = :year ORDER BY id DESC")
	List<InvoiceForFin2InProgressList> findByInvoiceTypeIdAndStatusNotAndMonth(Integer invoiceTypeId, String status, String month,String year);

    @Query("FROM InvoiceForFin2InProgressList WHERE stateId =:stateId AND invoiceTypeId = :invoiceTypeId AND status != :status AND status IS NOT NULL  AND status !='' AND month = :month AND year = :year ORDER BY id DESC")
	List<InvoiceForFin2InProgressList> findByInvoiceTypeIdAndStatusNotAndStateIdAndMonth(Integer invoiceTypeId, String status,Integer stateId, String month,String year);

    @Query("FROM InvoiceForFin2InProgressList WHERE stateId =:stateId AND districtId = :districtId AND invoiceTypeId = :invoiceTypeId AND status != :status AND status IS NOT NULL  AND status !='' AND month = :month AND year = :year ORDER BY id DESC")
	List<InvoiceForFin2InProgressList> findByInvoiceTypeIdAndStatusNotAndStateIdAndDistrictIdAndMonth(Integer invoiceTypeId, String status,Integer stateId,Integer districtId, String month,String year);

}

