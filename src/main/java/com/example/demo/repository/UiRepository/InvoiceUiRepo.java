package com.example.demo.repository.UiRepository;

import java.util.List;

import com.example.demo.EntityModel.UIEntityModels.InvoiceUi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InvoiceUiRepo extends JpaRepository<InvoiceUi,Integer> {

    @Query("FROM InvoiceUi WHERE invoiceTypeId = :invoiceTypeId AND status != :status AND status IS NOT NULL AND status !='' AND month = :month ORDER BY id DESC")
	List<InvoiceUi> findByInvoiceTypeIdAndStatusNotAndMonth(Integer invoiceTypeId, String status, String month);

    @Query("FROM InvoiceUi WHERE stateId =:stateId AND invoiceTypeId = :invoiceTypeId AND status != :status AND status IS NOT NULL  AND status !='' AND month = :month ORDER BY id DESC")
	List<InvoiceUi> findByInvoiceTypeIdAndStatusNotAndStateIdAndMonth(Integer invoiceTypeId, String status,Integer stateId, String month);

    @Query("FROM InvoiceUi WHERE stateId =:stateId AND districtId = :districtId AND invoiceTypeId = :invoiceTypeId AND status != :status AND status IS NOT NULL  AND status !='' AND month = :month ORDER BY id DESC")
	List<InvoiceUi> findByInvoiceTypeIdAndStatusNotAndStateIdAndDistrictIdAndMonth(Integer invoiceTypeId, String status,Integer stateId,Integer districtId, String month);

    
}
