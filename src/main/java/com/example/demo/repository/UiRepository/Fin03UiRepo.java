package com.example.demo.repository.UiRepository;

import com.example.demo.EntityModel.UIEntityModels.Fin03Ui;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface Fin03UiRepo extends JpaRepository<Fin03Ui,Integer> {
    

    @Query("FROM Fin03Ui WHERE id = :id")
	Fin03Ui findByFin03(Integer id);


    
}
