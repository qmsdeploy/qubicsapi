package com.example.demo.repository.UiRepository;

import java.util.List;

import com.example.demo.EntityModel.UIEntityModels.Fin08ForFin03Ui;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface Fin08ForFin03UiRepo extends JpaRepository<Fin08ForFin03Ui,Integer> {


    @Query("FROM Fin08ForFin03Ui WHERE status = 'APPROVED BY MOH' AND fin03_status IS NULL AND month = :month ORDER BY id DESC")
	List<Fin08ForFin03Ui> findByStatusAndFin03StatusIsNullAndMonth(String month);
	
	@Query("FROM Fin08ForFin03Ui WHERE status = 'APPROVED BY MOH' AND fin03_status IS NULL AND stateId = :stateId AND month = :month ORDER BY id DESC")
	List<Fin08ForFin03Ui> findByStatusAndFin03StatusIsNullAndStateIdAndMonth(Integer stateId, String month);
	
	@Query("FROM Fin08ForFin03Ui WHERE status = 'APPROVED BY MOH' AND fin03_status IS NULL AND stateId = :stateId AND districtId = :districtId AND month = :month ORDER BY id DESC")
	List<Fin08ForFin03Ui> findByStatusAndFin03StatusIsNullAndStateIdAndDistrictIdAndMonth(Integer stateId,Integer districtId, String month);

    List<Fin08ForFin03Ui> findByDistrictIdAndClinicTypeIdAndStatusAndFin03StatusIsNullAndMonthAndYear(Integer districtId, Integer clinicTypeId, String status,
			String month, String year);

}
