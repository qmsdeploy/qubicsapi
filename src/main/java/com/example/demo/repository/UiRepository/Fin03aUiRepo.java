package com.example.demo.repository.UiRepository;

import com.example.demo.EntityModel.UIEntityModels.Fin03aUi;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Fin03aUiRepo extends JpaRepository<Fin03aUi,Integer> {
    
}
