package com.example.demo.repository.UiRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.EntityModel.UIEntityModels.InvoiceForInvoiceReport;

public interface InvoiceForInvoiceReportRepository extends CrudRepository<InvoiceForInvoiceReport, Integer> {
	
	@Query("From InvoiceForInvoiceReport Where id = :id")
	InvoiceForInvoiceReport findByInvoiceId(Integer id);

	InvoiceForInvoiceReport findByInvoiceNo(String invoiceNo);

}
