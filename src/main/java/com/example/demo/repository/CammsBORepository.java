package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.EntityModel.CammsBO;

public interface CammsBORepository extends CrudRepository<CammsBO, Integer> {
	
	CammsBO findByBENumber(String BENumber);
	
//	@Query("FROM CammsBO WHERE fin_category = :fincategory AND is_migrated IS NULL AND beconditional_status IN ('ACT','PBR')")
//   List<CammsBO> findByFinCategoryAndIsMigratedIsNull(String fincategory);
//	
// 	List<CammsBO> findByFinCategory(String fincategory);
//	@Query(" FROM CammsBO WHERE id = :id")
//	CammsBO findByCammsBoId(Integer id);
//	
	@Query("FROM CammsBO WHERE beconditional_status != 'ACT' AND ownership = 'Purchase Biomedical'")
	List<CammsBO> findByCammsBO();
	
	List<CammsBO> findByIsFin06AndOwnershipAndIsFin06Migrated(char isFin06,String ownership, char isFin06Migrated);
	
	List<CammsBO> findByIsFin08cAndOwnershipAndIsFin08cMigrated(char isFin08c,String ownership,char isFin08cMigrated);
	
	List<CammsBO> findByIsFin07AndOwnershipAndIsFin07Migrated(char isFin07,String ownership,char isFin07Migrated);
	
	List<CammsBO> findByIsFin07AndOwnership(char isFin07,String ownership);
	
	List<CammsBO> findByIsFin08bAndOwnershipAndIsFin08bMigrated(char isFin08b,String ownership,char isFin08bMigrated);
	
	
	@Query(nativeQuery=true, value="SELECT * FROM cammsbo c WHERE is_fin08b ='Y' AND is_fin08b_migrated ='N' AND ownership = 'New Biomedical' LIMIT 600")
	List<CammsBO> findByCammsBO1();
	@Query(nativeQuery=true, value="SELECT * FROM cammsbo c WHERE is_fin07 ='Y' AND ownership ='New Biomedical' AND is_fin07migrated ='N' LIMIT 20000")
	List<CammsBO> findByCammsBO2();

}
