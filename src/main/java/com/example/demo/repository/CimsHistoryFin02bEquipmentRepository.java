package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.example.demo.EntityModel.CimsHistoryFin02bEquipment;
import com.example.demo.EntityModel.Invoice;

public interface CimsHistoryFin02bEquipmentRepository extends CrudRepository <CimsHistoryFin02bEquipment, Integer>{
	
	@Query("FROM CimsHistoryFin02bEquipment WHERE clinicTypeId = :clinicTypeId AND period >= :period AND year <= :year ORDER BY id ASC")
	List<CimsHistoryFin02bEquipment> findByClinicTypeAndPeriodAndYear(Integer clinicTypeId,String period, String year);
	
	@Query("FROM CimsHistoryFin02bEquipment WHERE stateId =:stateId AND clinicTypeId = :clinicTypeId AND period >= :period AND year <= :year ORDER BY id ASC")
	List<CimsHistoryFin02bEquipment> findByStateAndClinicTypeAndPeriodAndYear(Integer stateId,Integer clinicTypeId,String period, String year);
	
	@Query("FROM CimsHistoryFin02bEquipment WHERE stateId =:stateId AND districtId = :districtId AND clinicTypeId = :clinicTypeId AND year < :year ORDER BY id ASC")
	List<CimsHistoryFin02bEquipment> findByStateAndDistrictAndClinicTypeAndYear(Integer stateId, Integer districtId,Integer clinicTypeId, String year);
	
	@Query("FROM CimsHistoryFin02bEquipment WHERE stateId =:stateId AND districtId = :districtId AND clinicTypeId = :clinicTypeId AND period IN (:periods) AND year = :year ORDER BY id ASC")
	List<CimsHistoryFin02bEquipment> findByStateAndDistrictAndClinicTypeAndPeriodInAndYear(Integer stateId, Integer districtId,Integer clinicTypeId,List<String> periods, String year);

}
